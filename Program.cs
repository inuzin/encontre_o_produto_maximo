﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encontre_o_produto_maximo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { -6, 4, -5, 8, -10, 0, 8 };
            List<int> b = new List<int>();
            int inc = 0;

            //Preenche lista b sem o zero e calcula a quantidade de numeros negativos
            for (int i = 0; i <= a.Length - 1; i++)
            {
                if(a[i] != 0)
                b.Add(a[i]);

                if (a[i] < 0)
                {
                    inc += 1;
                }
            }

            //Sorteia os valores por ordem crescente
            b.Sort();

            //Caso a quantidade de numeros negativos seja impar
            if (!(inc % 2 == 0))
            {
                //Itera de forma decrescente ate encontrar o menor numero negativo em modulo, removendo-o
                for(int j = b.Count() - 1; j >= 0; j--)
                {
                    if (b[j] < 0)
                    {
                        b.RemoveAt(j);
                        break;
                    }                  
                }
            }

            Console.ReadLine();
        }
    }
}
